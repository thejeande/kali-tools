---
Title: firmware-sof
Homepage: https://github.com/thesofproject/sof-bin
Repository: https://salsa.debian.org/mpearson/firmware-sof
Architectures: all
Version: 2.2.2-1
Metapackages: kali-linux-firmware 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### firmware-sof-signed
 
  Provides the Intel SOF audio firmware and topology needed for audio
  functionality on some Intel system.
   
  This package contains the pre-built and signed binaries.
 
 **Installed size:** `15.92 MB`  
 **How to install:** `sudo apt install firmware-sof-signed`  
 
 {{< spoiler "Dependencies:" >}}
 * dpkg 
 {{< /spoiler >}}
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
